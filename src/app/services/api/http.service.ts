import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import axios from 'axios';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private api = environment.apiUrl;
  public isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  post(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.post(fullUrl, data)
  }

  get(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.get(fullUrl, data);
  }


  delete(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.delete(fullUrl, data);
  }

  put(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.put(fullUrl, data);
  }

  uploadImage(data: any) {
    let token: any = '';
    let user: any = localStorage.getItem('user');
    if (user) {
      user = JSON.parse(user);
      token = user.authorisation?.token;
    }
    return axios.post('http://192.168.0.60:8000/api/upload_profile_pic', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': 'bearer ' + token
      }
    })
  }
}
