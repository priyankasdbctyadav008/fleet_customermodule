import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpService } from './http.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private router: Router, private httpService: HttpService, private _snackBar: MatSnackBar) {
  }

  private _successMsgSource = new Subject<{ isSuccess: boolean, message: string }>();

  successMsg$ = this._successMsgSource.asObservable();


  sharedData: string = '';

  sendSuccessMsg(isSuccess: boolean, message: any) {
    const data = { isSuccess, message };
    console.log('data: ', data);
    this._successMsgSource.next(data);
  }

  setData(data: string) {
    this.sharedData = data;
    console.log('this.sharedData: ', this.sharedData);
  }

  getData(): string {
    console.log('this.sharedData: ', this.sharedData);
    return this.sharedData;
  }

  showSnackbar(message: string, isSuccess: boolean, durationInSeconds: number) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: durationInSeconds * 2000,
      data: {
        message: message,
        isSuccess: isSuccess,
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  async countryData(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get('country', '').subscribe({
          next: (res) => {
            console.log('res: ', res);
            resolve(res);
          }, error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);

      }
    });
  }


  async stateList(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`states/${value}`, '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async cityList(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`city/${value}`, '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  token() {
    let data: any = localStorage.getItem('user');
    if (data) {
      data = JSON.parse(data);
      return data.authorisation?.token;
    }
  }

  isLoggedIn() {
    if (this.token()) {
      return true;
    } else {
      return false;
    }
  }

  logIn(user: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('customer_login', user).subscribe({
          next: (res) => {
            localStorage.setItem('user', JSON.stringify(res));
            resolve(res);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          }
        })
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  getLocalStorage() {
    try {
      let data: any = localStorage.getItem('user');
      return JSON.parse(data)
    } catch (error) {
      return error;
    }
  }


  async contact_us(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.post(`contact_us`, value).subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('orgData');
    localStorage.removeItem('tabIndex');
    this.router.navigate(['/user/login']);
  }


  register(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('register', data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);

      }
    });
  }


  async sendOtp(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`resendOtp?email=${value}`, '').subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  async verifyEmailOtp(email: any, value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        let data = { email: email, otp: value };
        await this.httpService.post('verifyOtp', data).subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  async forgotPassowrd(email: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('forgot_password', email).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  vehicleLisyByorg(id: any, data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {

        this.httpService.isLoading.next(true);
        await this.httpService.get(`customer_vehicles_list?endDate=${data.endDate}&startDate=${data.startDate}&stateId=${data.stateId}&postalCode=${data.postalCode}&city=${data.city}&vehicleCategoryId=${data.vehicleCategory}&vehicleTypeId=${data.vehicleType}&organizationId=${id}`, '').subscribe({
          next: (response) => {

            resolve(response);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  getVehicleById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {

        await this.httpService.get(`agencyVehicle/${id}`, '').subscribe({
          next: (response) => {
            resolve(response);


          },
          error: (error) => {
            reject(error);

          },
        });
      } catch (error) {
        reject(error);

      }
    })
  }

  async organizationGetById(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`organization/${data}`, '').subscribe({
          next: (response) => {
            resolve(response);

          },
          error: (error) => {

            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async getVehicleTypeById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`vehiclesType/${id}`, '').subscribe({
          next: (res) => {
            resolve(res);

          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async getTypeDropDown(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`dropDown_vehiclesType`, '').subscribe({
          next: (res) => {

            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async getCategoryDropDown(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`dropDown_vehiclesCategory`, '').subscribe({
          next: (res) => {

            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async vehicleBrandDropdown(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`dropDown_vehiclesBrand`, '').subscribe({
          next: (res) => {
            resolve(res);

          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {

        reject(error);
      }
    });
  }

  async getBrandById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`vehiclesBrand/${id}`, '').subscribe({
          next: (res) => {

            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  updatedCustomerCompany(user: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.post(`update_customer_company/${id}`, user).subscribe({
          next: (res) => {
            resolve(res);
          }, error: (error) => {

            reject(error);
          }
        })
      } catch (error) {
        reject(error);
      }
    });
  }

  // User Profile Api
  async userById(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`user_profile/${data}`, '').subscribe({
          next: (response) => {
            resolve(response);

          },
          error: (error) => {

            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async uploadImage(files: any, userId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const formData = new FormData();
        formData.append('files[]', files);
        formData.append('user_id', userId);
        this.httpService.uploadImage(formData).then(response => {
          resolve(response);
        })
          .catch((error: any) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  async user_upcoming_bookings(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`user_upcoming_bookings/${id}`, '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async user_past_bookings(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`user_past_bookings/${id}`, '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async cutomerBooking(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.post(`create_customerBooking`, data).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (error) => {

            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }





}
