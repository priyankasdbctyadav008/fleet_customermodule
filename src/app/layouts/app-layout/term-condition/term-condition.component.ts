import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-term-condition',
  templateUrl: './term-condition.component.html',
  styleUrls: ['./term-condition.component.scss']
})
export class TermConditionComponent {

  title: any = [];
  constructor(private api: ApiService) {

  }
  ngOnInit() {
    this.api.sendSuccessMsg(false, this.title);
  }
}
