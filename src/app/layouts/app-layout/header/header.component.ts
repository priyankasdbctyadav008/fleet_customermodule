import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  Countries: any = [];
  isloggedIn = false;
  isloginUser: any;

  selectedTabIndex: any = 0;
  showFiller = false;
  opened: any;
  events: string[] = [];

  id: any;
  languages = [
    {
      id: 'en',
      title: 'English',
      sortname: "GB"

    },
    {
      id: 'fr',
      title: 'French',
      sortname: "FR"
    },
  ];
  initialLanguage = 'en';
  selectedLanguage: any
  currentLang: any;
  constructor(private api: ApiService, public translate: TranslateService, private router: Router) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang(this.initialLanguage);
  }

  ngOnInit() {
    console.log(this.selectedTabIndex)
    this.api.successMsg$.subscribe(
      (message) => {
        this.isloginUser = JSON.parse(JSON.stringify(message))?.isSuccess;
      }
    );

    this.selectedLanguage = this.languages[0];
    this.translate.use(this.languages[0].id);

    let user: any = localStorage.getItem('user');
    let data = JSON.parse(user);
    console.log('data: ', data);
    this.id = data?.user?.id;
    console.log('this.id: ', this.id);
  }


  logout() {
    this.successMsg(false)
    this.api.logout();
  }

  successMsg(value: any) {
    this.selectedTabIndex = 0;
    this.api.sendSuccessMsg(value, '');
  }

  logIn() {
    this.api.isLoggedIn();
  }

  switchLanguage(lang: any) {
    this.selectedLanguage = lang
    this.translate.use(lang.id);
  }

  countryList() {
    try {
      this.api.countryData().then((res: any) => {
        this.Countries = res.data;
      }, (error) => {
        console.log('error: ', error);
      });
    }
    catch (error) {
      console.log('error: ', error);
    }
  }

  onTabChange(event: any): void {
    localStorage.setItem('tabIndex', event.index);
    this.selectedTabIndex = event.index;
    switch (event.index) {
      case 0:
        this.router.navigate(['/']);
        break;
      case 1:
        this.router.navigate(['/my-booking']);
        break;
      case 2:
        this.router.navigate(['/user/contact-us'])
    }
  }
}
