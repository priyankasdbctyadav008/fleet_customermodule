import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent {
  title: any = [];

  constructor(private api: ApiService) {

  }
  ngOnInit() {
    this.api.sendSuccessMsg(false, this.title);
  }
}
