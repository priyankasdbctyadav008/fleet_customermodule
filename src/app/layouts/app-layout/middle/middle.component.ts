import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-middle',
  templateUrl: './middle.component.html',
  styleUrls: ['./middle.component.scss']
})
export class MiddleComponent {
  islogin=false;  
  userLogdin:any
  constructor(private router: Router) { }

  ngOnInit() {
    this.islogin=false; 
    this.userLogdin = localStorage.getItem('user')
    this.userLogdin = JSON.parse(this.userLogdin)?.success
    console.log('userLogdin: ',this.userLogdin);
    const currentUrl = this.router.url;
    const segments = currentUrl.split('/');
    const secondLastSegment = segments[segments.length - 1];
    console.log('secondLastSegment: ', secondLastSegment);
    if(this.userLogdin){
      if(secondLastSegment == 'vehicle-booking'){
        this.islogin = false; 
      }else{
        this.islogin=true; 
      }
    }
  }
  
}
