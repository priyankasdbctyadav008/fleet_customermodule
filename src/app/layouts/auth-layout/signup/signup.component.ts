import { Component } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmPasswordValidator } from 'validator';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  registerForm: FormGroup;
  submitted: boolean = false;
  confirmPassword: any;
  password: any
  errorMsg: any;
  disabled: boolean = false;
  durationInSeconds = 1;
  data: any;
  loader = true;
  id: any;
  checkboxElement: any

  constructor(private fb: FormBuilder, private router: Router, private api: ApiService) {
    this.registerForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      phone: ['', Validators.compose([Validators.required, Validators.maxLength(18)])],
      role: ['3'],
      image: ['null'],
      status: [false, Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")])],
      confirmPassword: ['', Validators.required],
      organizationId: ['1', Validators.required]
    },
      {
        validators: ConfirmPasswordValidator('password', 'confirmPassword'),
      } as AbstractControlOptions
    );
  }

  ngOnInit() {
    this.id = localStorage.getItem('organizationId') ? localStorage.getItem('organizationId') : '1';
    this.successMsg(false);
    setTimeout(() => {
      this.loader = false;
    }, 2000);
  }

  confirmPasswordVisibilty() {
    this.confirmPassword = !this.confirmPassword;
  }

  passwordVisibilty() {
    this.password = !this.password;
  }

  successMsg(value: any) {
    let data: any = ['TITLE.CAR_RENTAL_SERVICE', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT']
    this.api.sendSuccessMsg(value, data);
  }

  register() {
    let data = this.registerForm.value;
    this.checkboxElement = document.getElementsByClassName('status_check');
    if (!this.registerForm.value.status) {
      if (this.checkboxElement) {
        this.checkboxElement[0].classList.add('mat-checkbox-color');
      }
    }
    if (this.registerForm.valid) {
      try {
        data.organizationId = this.id
        this.api.register(data).then((res: any) => {
          if (res.success === true) {
            this.api.showSnackbar(res.message, true, this.durationInSeconds);
            localStorage.setItem('userEmail', this.registerForm.value.email);
            this.api.sendOtp(data.email).then((data) => {
              this.api.showSnackbar(data.message, true, this.durationInSeconds);
              this.router.navigate(['/user/verify-email']);
            })
          } else {
            this.api.showSnackbar(res.message, false, this.durationInSeconds);
          }
        }, (error) => {
          this.api.showSnackbar(error.message, false, this.durationInSeconds);
        });
      } catch (error: any) {
        this.api.showSnackbar(error, false, this.durationInSeconds);
      }
    }
  }

}
