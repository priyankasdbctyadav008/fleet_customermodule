import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgOtpInputComponent } from 'ng-otp-input';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent {
  @ViewChild(NgOtpInputComponent, { static: false }) ngOtpInput!: NgOtpInputComponent;
  title: any = ['TITLE.CAR_RENTAL_SERVICE', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT'];
  email = localStorage.getItem('userEmail');
  durationInSeconds = 1;
  constructor(private api: ApiService, private router: Router) {

  }

  ngOnInit() {
    this.api.sendSuccessMsg(false, this.title);
  }

  onOtpChange(value: any) {
    try {
      let data = value.split('')
      if (data.length === 4) {
        this.ngOtpInput.otpForm.disable();
        this.api.verifyEmailOtp(this.email, value).then((res: any) => {
            if (res.success === true) {
              this.api.showSnackbar(res.message, true, this.durationInSeconds);
              this.router.navigate(['']);
            } else if (res.success === false) {
              this.api.showSnackbar('Invalid Otp', false, this.durationInSeconds);
              this.ngOtpInput.setValue('');
              this.ngOtpInput.otpForm.enable();
              data = null;
            }
          }, (error: any) => {

          })
      }
    } catch (error) {
      console.error(error);
    }
  }


  resendOtp() {
    try {
      this.api.sendOtp(this.email).then((data) => {
        this.api.showSnackbar(data.message, true, this.durationInSeconds);
      }, (error) => {
        this.api.showSnackbar('Sonething went worng', false, this.durationInSeconds);
      })
    } catch (error) {
      console.error(error);
    }
  }
}




