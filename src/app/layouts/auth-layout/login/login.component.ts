import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/services/api/api.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [TranslatePipe]
})

export class LoginComponent {
  loginForm: FormGroup;
  submitted: boolean = false;
  showPassword: any;
  password: any
  errorMsg: any;
  disabled: boolean = false;
  durationInSeconds = 1;
  data: any;
  id: any;
  loader = true;
  organizationId: any

  constructor(private fb: FormBuilder, private router: Router, public translate: TranslateService,
    public translatePipe: TranslatePipe, private api: ApiService) {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      password: ['', Validators.required],
      organizationId: ['1', Validators.required]
    });
  }

  ngOnInit() {
    this.successMsg(false);
    this.organizationId = localStorage.getItem('organizationId') ? localStorage.getItem('organizationId') : '1';
    let userData = localStorage.getItem('user');
    if (userData) {
      localStorage.removeItem('user');
    }
    setTimeout(() => {
      this.loader = false;
    }, 2000);
  }

  toggleVisibility() {
    this.showPassword = !this.showPassword;
  }


  login() {
    let data = this.loginForm.value;
    data.organizationId = this.organizationId;
    if (this.loginForm.valid) {
      this.api.logIn(data).then((res) => {
        if (res.success === true) {
          if (res.user.Isemail_verified == '0') {
            localStorage.setItem('user', JSON.stringify(res.user));
            localStorage.setItem('userEmail', this.loginForm.value.email);
            this.api.sendOtp(this.loginForm.value.email).then((data) => {
              this.api.showSnackbar('Mail Send', true, this.durationInSeconds);
              this.router.navigate(['/user/verify-email']);
            })
          }
          else {
            if (res.user.role == '3') {
              this.api.showSnackbar('Login Successfully', true, this.durationInSeconds);
              this.userData();
              this.successMsg(true);
              this.router.navigate([''])
            }
          }
        } else {
          console.log('res.message: ', res.message);
          this.api.showSnackbar(res.message, false, this.durationInSeconds);
        }
      }, (error) => {
        this.api.showSnackbar('Server Error', false, this.durationInSeconds);
      })
    } else {
      this.api.showSnackbar('This field is required', false, this.durationInSeconds)
    }
  }


  successMsg(value: any) {
    let data: any = ['TITLE.CAR_RENTAL_SERVICE', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT']
    this.api.sendSuccessMsg(value, data);
  }

  async userData() {
    this.data = await this.api.getLocalStorage();
    this.id = this.data.user.id;

  }

}