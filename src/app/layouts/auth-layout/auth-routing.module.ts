import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from 'src/app/main/profile/profile.component';
import { PrivacyPolicyComponent } from '../app-layout/privacy-policy/privacy-policy.component';
import { TermConditionComponent } from '../app-layout/term-condition/term-condition.component';
import { ContactUsComponent } from 'src/app/main/contact-us/contact-us.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PageNotFoundComponent } from 'src/app/helper/page-not-found/page-not-found.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    title: "login fleet Management"
  },
  {
    path: 'register',
    component: SignupComponent,
    title: "register fleet Management"
  },
  {
    path: 'profile/:id',
    component: ProfileComponent,
    title: "profile fleet Management"
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    title: "privacy policy fleet Management"
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    title: "forgot password fleet Management"
  },
  {
    path: 'terms-condition',
    component: TermConditionComponent,
    title: "terms condition fleet Management"
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    title: "contact us fleet Management"
  },
  {
    path: 'verify-email',
    component: VerifyEmailComponent,
    title: "verify email fleet Management"
  },
  {
    path: '404',
    pathMatch: 'full',
    component: PageNotFoundComponent
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
