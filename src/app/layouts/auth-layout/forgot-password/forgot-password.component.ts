import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  forgotPasswordForm: FormGroup;
  errorMsg: any;
  disabled: boolean = false;
  durationInSeconds = 1;
  data: any;
  loader: any;
  title: any = ['TITLE.CAR_RENTAL_SERVICE', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT']

  constructor(private fb: FormBuilder, public api: ApiService, private router: Router) {
    this.forgotPasswordForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])]
    })
  }

  ngOnInit() {
    this.api.sendSuccessMsg(false, this.title);
    setTimeout(() => {
      this.loader = false;
    }, 2000);
  }

  forgot() {
    try {
      if (this.forgotPasswordForm.valid) {
        this.api.forgotPassowrd(this.forgotPasswordForm.value).then((res: any) => {
          if (res.success) {
            this.api.showSnackbar(res.message, true, this.durationInSeconds);
            this.router.navigate(['/user/login']);
          } else if (res.success === false) {
            this.api.showSnackbar(res.message, false, this.durationInSeconds);
            this.forgotPasswordForm.reset();
          }
        }, (error) => {
          this.forgotPasswordForm.reset();
          this.api.showSnackbar(error, false, this.durationInSeconds);
        });
      }
    }
    catch (error: any) {
      this.api.showSnackbar(error, false, this.durationInSeconds);
    }
  }
}