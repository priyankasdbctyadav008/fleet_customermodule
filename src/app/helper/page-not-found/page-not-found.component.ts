import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent {
  title: any = [];

  constructor(private api: ApiService) {

  }
  ngOnInit() {
    this.api.sendSuccessMsg(false, this.title);
  }
}
