import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { VehiclePaymentComponent } from 'src/app/main/vehicle-booking/vehicle-payment/vehicle-payment.component';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent {

  constructor(public dialogRef: MatDialogRef<VehiclePaymentComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private api: ApiService){

  }
}
