import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent {
  contactForm: FormGroup;
  errorMsg: any;
  disabled: boolean = false;
  isLoggedIn: any = false;
  durationInSeconds = 1;
  data: any;
  duration = 1;
  title: any = ['TITLE.CONTACT_US', 'TITLE.HOME_CONTACT']
  constructor(private fb: FormBuilder, public api: ApiService, private router: Router) {
    this.contactForm = this.fb.group({
      full_name: ['', Validators.required],
      phone_number: ['',Validators.compose([Validators.required, Validators.maxLength(18)])],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      message: ['', Validators.required],
      user_id: [''],
    })
  }
  ngOnInit() {
    this.isLogIn();
    this.userData();
    this.api.sendSuccessMsg(this.isLoggedIn, this.title);
  }


  isLogIn() {
    this.isLoggedIn = this.api.isLoggedIn();
  }

  async userData() {
    this.data = await this.api.getLocalStorage();
    if (this.data) {
      this.contactForm.controls['user_id'].setValue(JSON.stringify(this.data.user.id))
    }
  }


  submit() {
    let data = this.contactForm.value;
    try {
      this.api.contact_us(data).then((res: any) => {
        if (res.success === true) {
          this.api.showSnackbar(res.message, true, this.durationInSeconds);
        }
      }, (error: any) => {
        this.api.showSnackbar('87787878', false, this.durationInSeconds);
        console.log('error: ', error);
      })
    } catch (error:any) {
      this.api.showSnackbar('889898989898', false, this.durationInSeconds);
      console.log('error: ', error);
    }
  }

}
