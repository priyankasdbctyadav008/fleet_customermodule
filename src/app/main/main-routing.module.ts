import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [{
  path: ':id/vehicle',
  loadChildren: () => import('../main/vehicle-booking/vehicle.module').then(m => m.VehicleModule),
  title: "booking-Fleet Management",

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
