import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-booking',
  templateUrl: './my-booking.component.html',
  styleUrls: ['./my-booking.component.scss'],
  providers: [DatePipe]
})
export class MyBookingComponent {
  id: any;
  state: any
  data: any;
  vehicleBrand: any;
  vehicleType: any;
  userId: any;
  city: any;
  cityName: any;
  cityData: any;
  upcomingBookings: any;
  pastBookings: any;
  customerBooking: any = [];
  numbers: any;
  loader = true;
  title: any = ['TITLE.BOOKINGS', 'TITLE.HOME_BOOKINGS'];
  imageUrl = environment.imageUrl;
  constructor(public api: ApiService, private route: ActivatedRoute, public date: DatePipe) {
    let data = this.api.getLocalStorage();
    this.userId = data?.user?.id;

  }


  ngOnInit() {
    this.api.sendSuccessMsg(true, this.title);
    this.id = 1;
    this.getUpcomingBookings(this.userId);

    setTimeout(() => {
      this.loader = false;
    }, 3000);

  }

  maskNumber(number: any) {
    if (number.length > 8) {
      const last4Digits = number.slice(-4);
      const maskedPart = 'x'.repeat(number.length - 4);
      this.numbers = `${maskedPart.slice(0, 4)}-${maskedPart.slice(4, 8)}-${maskedPart.slice(8, 12)}-${last4Digits}`;
      return this.numbers
    }

    return number
  }

  getImage(imageName: string): string {
    // return `http://192.168.0.60/fleet-management/api/public/uploads/${imageName}`;
    return `${this.imageUrl}/${imageName}`;
  }

  async getUpcomingBookings(id: any) {
    try {
      await this.api.user_upcoming_bookings(id).then((res: any) => {
        if(res.success === true) {
          this.upcomingBookings = res.data;
          console.log('this.upcomingBookings: ', this.upcomingBookings.VehicleTypeId);
          this.upcomingBookings.map((value: any) => {
            if (value.VehicleTypeId) {
              this.getTypeById(value.VehicleTypeId)
            }
            if (value.stripeTransactionDetails) {
              value.stripeTransactionDetails = JSON.parse(value.stripeTransactionDetails);
            }
            if (value.stateId) {
              this.cityList(value.stateId, this.upcomingBookings);
            }
          })
          console.log('this.upcomingBookings: ', this.upcomingBookings);

        } else {
          console.log('res: ', res);
        }
      }, (error: any) => {
        console.log('error: ', error);

      })
    } catch (error) {
      console.log('error: ', error);

    }
  }

  getPastBookings(id: any) {
    try {
      this.api.user_past_bookings(id).then((res: any) => {
        if (res.success === true) {
          this.pastBookings = res.data;
          console.log('his.pastBookings: ', this.pastBookings);
          let stripedate = this.pastBookings[0].stripeTransactionDetails;
          console.log('this.customerBooking: ', this.customerBooking);
          this.pastBookings.map((value: any) => {
            if (value.stateId) {
              this.cityList(value.stateId, this.pastBookings);
            }
          })
          console.log('this.pastBookings: ', this.pastBookings);
        } else {
         
          console.log('res: ', res);
        }

      }, (error: any) => {
        console.log('error: ', error);

      })
    } catch (error) {
      console.log('error: ', error);

    }
  }


  getTypeById(id: any) {
    try {
      this.api.getVehicleTypeById(id).then((res: any) => {
        const brandName = res.data;
        this.vehicleType = brandName.name;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  onTabChange(event: any) {
    console.log('event: ', event);
    if (event.index === 0) {
     // this.customerBooking = [];
      console.log('event.index: ', event.index);
      this.getUpcomingBookings(this.userId);
    } else if(event.index === 1) {
      this.customerBooking = [];
      console.log('event.index: ', event.index);
      this.getPastBookings(this.userId)
    }
  }


  getBrandById(id: any) {
    try {
      this.api.getBrandById(id).then((res: any) => {
        const brandName = res?.data?.vehiclesBrandMst;
        this.vehicleBrand = brandName?.name
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  cityList(stateId: any, value: any) {
    this.customerBooking = [''];
    console.log('this.customerBooking : ', this.customerBooking );
    console.log('value: ', value);
    try {
      this.api.cityList(stateId).then(
        (res: any) => {
          this.city = res.data;
          for (let i = 0; i < value.length; i++) {
            this.city.map((city: any) => {
              if (value[i].city === city.id) {
                value[i].city = city.name
              }
            });
          }
          this.customerBooking = value;
          console.log('this.customerBooking : ', this.customerBooking );
        },
        (error) => {
          console.error('error: ', error);
        }
      );

    } catch (error) {
      console.error(error);
    }

  }


  stateList(value: any) {
    try {
      this.api.stateList(value).then(
        (res: any) => {
          this.state = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }
}
