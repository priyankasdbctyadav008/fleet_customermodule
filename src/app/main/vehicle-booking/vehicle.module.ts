import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { VehicleRoutingModule } from './vehicle-routing.module';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { VehicleDetailComponent } from './vehicle-detail/vehicle-detail.component';
import { VehiclePaymentComponent } from './vehicle-payment/vehicle-payment.component';
import { MyBookingComponent } from './my-booking/my-booking.component';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    VehicleListComponent,
    VehicleDetailComponent,
    VehiclePaymentComponent,
    MyBookingComponent,
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    VehicleRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    SlickCarouselModule,
    NgxMaskDirective,
    NgxMaskPipe
  ]
})
export class VehicleModule { }
