import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/services/api/api.service';
import { DatePipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss'],
  providers: [DatePipe]


})
export class VehicleListComponent {
  vehicleSearch: any = FormGroup;
  Countries: any = [];
  city: any = [];
  submitted: boolean = false;
  showPassword: any;
  password: any
  errorMsg: any;
  disabled: boolean = false;
  durationInSeconds = 1;
  State: any;
  vehicleCategory: any;
  vehicles: any = [];
  vehicleType: any;
  vehicleBrand: any;
  orgId: any;
  vehicleToUpdate: any;
  slides: any = [];
  title: any = ['TITLE.CAR_SEARCH_BOOK', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT'];
  imageUrl = environment.imageUrl;
  noOfVehicle: any;
  isLoggedIn: any = false;

  constructor(private fb: FormBuilder, private router: Router, public translate: TranslateService,
    public api: ApiService, private route: ActivatedRoute, private date: DatePipe) {
    this.vehicleSearch = this.fb.group({
      stateId: [''],
      countryId: [''],
      city: [''],
      postalCode: [''],
      vehicleType: [''],
      vehicleCategory: [''],
      startDate: [''],
      endDate: ['']
    });
  }


  ngOnInit() {
    this.isLogIn();
    this.api.sendSuccessMsg(this.isLoggedIn, this.title);
    let orgData = localStorage.getItem('organizationId');
    let orgIdRoute =  this.route.snapshot.queryParams['orgId']
    if (!orgData && !orgIdRoute) {
      this.router.navigate(['404'])
    } else {
      this.orgId = this.route.snapshot.queryParams['orgId'] ? this.route.snapshot.queryParams['orgId'] : orgData;
      if (this.orgId) {
        localStorage.setItem('organizationId', this.orgId)
        this.organizationGetById(this.orgId);
      };
    }
    this.vehicleList();
    this.categoryDropDown();
    this.typeDropDown();
  }

  onchange(value: any) {
    console.log('value: ', value);
  }

  isLogIn() {
    this.isLoggedIn = this.api.isLoggedIn();
  }


  typeDropDown() {
    try {
      this.api.getTypeDropDown().then((res: any) => {
        this.vehicleType = res.data;
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }


  categoryDropDown() {
    try {
      this.api.getCategoryDropDown().then((res: any) => {
        this.vehicleCategory = res.data;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  brandDropDown() {
    try {
      this.api.vehicleBrandDropdown().then((res: any) => {
        this.vehicleBrand = res.data;
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }

  getBrandById(id: any) {
    try {
      this.api.getBrandById(id).then((res: any) => {
        const brandName = res.data.vehiclesBrandMst;
        this.vehicleToUpdate = this.vehicles.find((vehicle: any) => vehicle.vehicleBrandId === brandName.id);

        if (this.vehicleToUpdate) {
          this.vehicleToUpdate.vehicleBrandId = brandName.name;
        }
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  stateList(value: any) {
    try {
      this.api.stateList(value).then(
        (res: any) => {
          this.State = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  cityList(value: any) {
    try {
      this.api.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }


  vehicleList() {
    try {
      let data = this.vehicleSearch.value;
      data.startDate = data.startDate ? new Date(data.startDate).toISOString() : data.startDate;
      data.endDate = this.date.transform(data.endDate, 'yyyy-mm-dd');
      console.log('this.orgId:1111111 ', this.orgId);
      this.api.vehicleLisyByorg(this.orgId, data).then((res: any) => {
        this.vehicles = res.data;
        this.noOfVehicle = res.total_count;
        this.slides = this.vehicles;
        this.vehicles?.map((vehicle: any) => {
          this.getBrandById(vehicle.vehicleBrandId);
          const motor = JSON.parse(vehicle.motorization);
          vehicle.motorization = Object.keys(motor);
          const gearjson = JSON.parse(vehicle.gearBox);
          vehicle.gearBox = Object.keys(gearjson);

        });
      });
    } catch (error) {
      console.log('error: ', error);

    }
  }


  getImage(imageName: string): string {
    if (imageName) {
      return `${this.imageUrl}/${imageName}`;
    } else {
      return `../../../assets/images/car-missMatch.svg`;
    }
  }


  organizationGetById(id: any) {
    try {
      this.api.organizationGetById(id).then(
        (res: any) => {
          let response = {
            ...res.data.organization,
            ...res.data.user
          }
          this.stateList(response.countryId);
          localStorage.setItem('countryId', (response.countryId));
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  slideConfig = {
    "slidesToShow": 2, "slidesToScroll": 1, margin: "5px", arrows: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
    ],
  };

  resetForm(){
    this.vehicleSearch.reset();
    window.location.reload();
  }

  slickInit(e: any) {
    console.log('slick initialized');
  }

  breakpoint(e: any) {
    console.log('breakpoint');
  }

  afterChange(e: any) {
    console.log('afterChange');
  }

  beforeChange(e: any) {
    console.log('beforeChange');
  }

}
