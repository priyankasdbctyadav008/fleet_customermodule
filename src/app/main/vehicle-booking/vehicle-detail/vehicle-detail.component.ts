import { DatePipe, Location } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/services/api/api.service';
import { environment } from 'src/environments/environment';



@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.scss'],
  providers: [DatePipe]
})
export class VehicleDetailComponent {
  vehicleId: any;
  vehicleData: any = [];
  vehicleBrand: any;
  vehicleDetail: FormGroup;
  state: any;
  city: any;
  date = new Date();
  currentYear = this.date.getFullYear();
  minDate: any = Date;
  minenddate: any = Date;
  title: any = ['TITLE.CAR_SEARCH_BOOK', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT', ''];
  imageUrl = environment.imageUrl;
  slide: any = [];

  constructor(private fb: FormBuilder, private router: Router, public translate: TranslateService,
    public api: ApiService, private route: ActivatedRoute, private datepipe: DatePipe, private location: Location) {
    this.vehicleDetail = this.fb.group({
      stateId: ['', Validators.required],
      city: ["", Validators.required],
      postalCode: ['', Validators.required],
      bookingendDate: ['', Validators.required],
      bookingstartDate: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.api.sendSuccessMsg(true, this.title);
    this.vehicleId = this.route.snapshot.params['vehicleId'];
    if (this.vehicleId) {
      this.getVehicle();
    }
    let countryId = localStorage.getItem('countryId');
    this.stateList(countryId);
    this.minDate = new Date(this.currentYear, this.date.getMonth(), this.date.getDate());
    console.log(' this.minDate : ', this.minDate);
  }

  back() {
    this.location.back();
  }



  start(event: any) {
    this.minenddate = event.value;
  }

  setData() {
    if (this.vehicleDetail.valid) {
      let data: any = {
        ...this.vehicleDetail.value,
        ...this.vehicleData
      }
      this.router.navigate([`vehicle-payment/${this.vehicleData.id}`]);
      this.api.setData(data);
    } else {
      this.vehicleDetail.markAllAsTouched()
    }
  }

  getImage(imageName: string) {
    return `${this.imageUrl}/${imageName}`;
  }


  getBrandById(id: any) {
    try {
      this.api.getBrandById(id).then((res: any) => {
        const brandName = res.data.vehiclesBrandMst;
        this.vehicleBrand = brandName.name;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  getVehicle() {
    try {
      this.api.getVehicleById(this.vehicleId).then((res: any) => {
        let response = res.data.agencyVehicles;
        this.vehicleData = response;
        this.slide = [
          { img: this.vehicleData.image_url1 },
          { img: this.vehicleData.image_url2 },
          { img: this.vehicleData.image_url3 },
          { img: this.vehicleData.image_url4 },
          { img: this.vehicleData.image_url5 }
        ]
        console.log(' this.slide: ', this.slide);
        const motor = JSON.parse(this.vehicleData.motorization);
        this.vehicleData.motorization = Object.keys(motor);
        const gearjson = JSON.parse(this.vehicleData.gearBox);
        this.vehicleData.gearBox = Object.keys(gearjson);
        this.getBrandById(this.vehicleData.vehicleBrandId);
      }, (error) => {
        console.error('error: ', error);
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  stateList(value: any) {
    try {
      this.api.stateList(value).then(
        (res: any) => {
          this.state = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  cityList(value: any) {
    try {
      this.api.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  async onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }


  slideConfig = {
    "slidesToShow": 3, "slidesToScroll": 1, margin: "0px",
    "centerMode": true, "centerPadding": '50px',
    dots: false,
    infinite: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
        }
      },
    ],
  };


  slickInit(e: any) {
    console.log('slick initialized');

  }

  breakpoint(e: any) {
    console.log('breakpoint');
  }

  afterChange(e: any) {
    console.log('afterChange');

  }

  beforeChange(e: any) {
    console.log('beforeChange');
  }
}
