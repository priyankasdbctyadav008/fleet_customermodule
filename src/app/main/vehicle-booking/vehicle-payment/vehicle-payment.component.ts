import { DatePipe, Location } from '@angular/common';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormatNumberOptions } from 'libphonenumber-js';
import { DialogBoxComponent } from 'src/app/helper/dialog-box/dialog-box.component';
import { ApiService } from 'src/app/services/api/api.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-vehicle-payment',
  templateUrl: './vehicle-payment.component.html',
  styleUrls: ['./vehicle-payment.component.scss'],
  providers: [DatePipe]
})
export class VehiclePaymentComponent {
  bookingForm: FormGroup;
  vehicleId: any;
  vehicleData: any = [];
  vehicleBrand: any;
  cityName: any;
  data: any;
  city: any;
  State: any;
  userId: any;
  organizationId: any;
  durationInSeconds = 1;
  title: any = ['TITLE.CAR_SEARCH_BOOK', 'TITLE.LOCATION', 'TITLE.CUSTOMER_SUPPORT', '']
  imageUrl = environment.imageUrl;

  constructor(private route: ActivatedRoute, public dialog: MatDialog, private api: ApiService,
    private location: Location, private fb: FormBuilder, private date: DatePipe) {
    this.bookingForm = this.fb.group({
      paymentGateWayId: [0],
      amount: [],
      paymentType: ['card'],
      stripeTransactionDetails: this.fb.group({
        customerId: ["custo_12o"],
        card_number: ['', Validators.required],
        expiry_date: ['', Validators.required],
        cvv: ['', Validators.required]
      }),
    });
  }

  ngOnInit() {
    this.api.sendSuccessMsg(true, this.title);
    this.getData();
    this.organizationId = localStorage.getItem('countryId');
    let userData: any = localStorage.getItem('user');
    this.userId = JSON.parse(userData).user.id;
  }


  back() {
    this.location.back();

  }

  getImage(image: any) {
    // return `http://192.168.0.60/fleet-management/api/public/uploads/${image}`;
    return `${this.imageUrl}/${image}`;
  }

  getData() {
    this.data = this.api.getData()
    this.getBrandById(this.data?.vehicleBrandId);
    this.cityList(this.data?.stateId);
    console.log('data: ', this.data);
  }

  cityList(value: any) {
    console.log('value: ', value);
    try {
      this.api.cityList(value).then(
        (res: any) => {
          this.city = res.data;
          this.city?.map((city: any) => {
            if (this.data.city === city.id) {
              this.cityName = city.name;
            }
          });
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  stateList(value: any) {
    try {
      this.api.stateList(value).then(
        (res: any) => {
          this.State = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  getBrandById(id: any) {
    try {
      this.api.getBrandById(id).then((res: any) => {
        const brandName = res?.data?.vehiclesBrandMst;
        this.vehicleBrand = brandName?.name
        console.log(' this.vehicleBrand: ', this.vehicleBrand);


      });
    } catch (error) {
      console.log('error: ', error);
    }
  }



  submit() {

    let response: any = {
      ...this.bookingForm.value,
      ...this.data
    }


    const selectedstartDate = response.bookingstartDate;
    response.bookingstartDate = this.date.transform(selectedstartDate, 'yyyy-MM-dd HH:mm:ss');
    const selectedendDate = response.bookingendDate;
    response.bookingendDate = this.date.transform(selectedendDate, 'yyyy-MM-dd HH:mm:ss');
    response.stripeTransactionDetails = JSON.stringify(response.stripeTransactionDetails);
    response.amount = response.ratePerDay;
    response['agencyVehicleId'] = response.id;
    response['userId'] = this.userId;
    response['organizationId'] = this.organizationId;

    try {
      this.api.cutomerBooking(response).then((res: any) => {
        if (res.success === true) {
          this.openDialog(res.message);
          console.log('res: ', res);
        } else {
          this.api.showSnackbar(res.message, false, this.durationInSeconds);
        }

      }, (error: any) => {
        console.log('error: ', error);

      })
    } catch (error) {
      console.log('error: ', error);

    }
  }

  openDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      data: data,

    });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }
}
