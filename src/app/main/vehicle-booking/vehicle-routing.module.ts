import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { VehicleDetailComponent } from './vehicle-detail/vehicle-detail.component';
import { VehiclePaymentComponent } from './vehicle-payment/vehicle-payment.component';
import { MyBookingComponent } from './my-booking/my-booking.component';
import { PageNotFoundComponent } from 'src/app/helper/page-not-found/page-not-found.component';
import { AuthGuard } from 'src/app/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: VehicleListComponent,
    title: "Vehicle-List "
  },
  {
    path: 'vehicle-detail/:vehicleId',
    component: VehicleDetailComponent,
    canActivate: [AuthGuard],
    title: "Vehicle-Detail"
  },
  {
    path: 'my-booking',
    component: MyBookingComponent,
    canActivate: [AuthGuard],
    title: "Booking"
  },
  {
    path: 'vehicle-payment/:vehicleId',
    component: VehiclePaymentComponent,
    canActivate: [AuthGuard],
    title: "Vehicle-Payment"
  },
  {
    path: '404',
    pathMatch: 'full',
    component: PageNotFoundComponent
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleRoutingModule { }
