import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { AngularMaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthRoutingModule } from '../layouts/auth-layout/auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMatIntlTelInputComponent } from 'ngx-mat-intl-tel-input';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HeaderComponent } from '../layouts/app-layout/header/header.component';
import { FooterComponent } from '../layouts/app-layout/footer/footer.component';
import { MiddleComponent } from '../layouts/app-layout/middle/middle.component';
import { AuthGuard } from '../auth.guard';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    TranslateModule
  ],
  providers: [AuthGuard],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainModule { }
