import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmPasswordValidator } from 'validator';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],

})
export class ProfileComponent {
  profile: FormGroup
  state: any;
  city: any
  countries: any;
  email: any;
  id: any
  userData: any;
  profile_data: any;
  durationInSeconds = 1;
  imageUrl: string | ArrayBuffer | any;
  selectedFile: File | null = null;
  isLoggedIn: any = false
  title: any = ['TITLE.PROFILE', 'TITLE.HOME_PROFILE'];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private api: ApiService
  ) {
    this.profile = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      phone: ['', Validators.compose([Validators.required, Validators.maxLength(18)])],
      profile_pic: [""],
      name: [''],
      company_address: [''],
      company_countryId: [''],
      company_stateId: [''],
      company_city: [''],
      company_postalCode: [''],
      sirenno: [''],
      role: ['3'],
      status: [true],
    });
  }

  ngOnInit() {
    this.isLogIn();
    this.api.sendSuccessMsg(this.isLoggedIn, this.title);
    this.profile.disable();
    console.log(' this.profile.disable(): ',  this.profile.disable());
    this.countryList();
    this.id = this.route.snapshot.params['id'];
    if (this.id) {
      this.userById(this.id);
    }
  }


  isLogIn() {
    this.isLoggedIn = this.api.isLoggedIn();
    console.log(' this.isLoggedIn: ', this.isLoggedIn);
  }

  validForm() {
    console.log('this.profile: ', this.profile.enable());
    this.profile.enable();
    this.profile.controls['email'].disable();
  }

  userById(id: any) {
    try {
      this.api.userById(id).then(
        (res: any) => {
          let response = {
            ...res.data.user_details,
            ...res.data.company_details
          }
          this.userData = response;
          const userFromValue = this.profile.value;
          this.stateList(this.userData.company_countryId);
          this.cityList(this.userData.company_stateId);
          Object.keys(userFromValue).forEach((key) => {
            try {
              if (key === 'email') {
                this.profile.controls[key].setValue(this.userData[key]);

              } else if (key === 'profile_pic') {
                let url = this.userData.profile_pic;
                this.displayImage(url);
                this.profile.controls[key].setValue(this.userData[key]);
              }
              else {
                this.profile.controls[key].setValue(this.userData[key]);

              }
            } catch (error) {
              console.error(' failed at ' + key + ' ' + error);
            }
          });
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  displayImage(src: any) {
    console.log('src: ', src);
    let srcUrl: any;
    if (src) {
      srcUrl = `http://192.168.0.60/fleet-management/api/public/uploads/${src}`;
    } else {
      srcUrl = "../../../../assets/images/profile.jpg"
    }
    this.imageUrl = srcUrl;
  }


  submit() {
    let data = this.profile.value;
    data['email'] = this.profile.controls['email'].value;
    if (this.profile.valid) {
      try {
        this.api.updatedCustomerCompany(data, this.id).then((res: any) => {
          console.log('res: ', res);
          if (res.success == true) {
            this.api.showSnackbar(res.message, true, this.durationInSeconds);
          }
        }, (error) => {
          console.error('error: ', error);
        })
      } catch (error) {
        console.error('error: ', error);
      }
    }
  }



  imageUpload(event: any, id: any) {
    try {
      const file: File = event.target.files[0];
      if (file) {
        this.api.uploadImage(file, id).then((res: any) => {
          this.profile_data = res.data.data[0].name;
          this.displayImage(this.profile_data)
          this.profile.controls['profile_pic'].setValue(this.profile_data);
        }, (error): any => {
          console.log('error: ', error);
        })
      }
    } catch (error) {
      console.log('error: ', error);
    }
  }


  async countryList() {
    try {
      this.api.countryData().then(
        (res: any) => {
          this.countries = res.data;
        },
        (error) => {
          console.error('error', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  stateList(value: any) {
    try {
      this.api.stateList(value).then(
        (res: any) => {
          this.state = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  cityList(value: any) {
    try {
      this.api.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  async onChangeState(value: any) {
    try {
      this.stateList(value);
    } catch (error) {
      console.error(error);
    }
  }


  async onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }

}
