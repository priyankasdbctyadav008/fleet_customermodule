import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { LoginGuard } from './login.guard';


const routes: Routes = [
  {
    path: 'user',
    loadChildren: () => import('./layouts/auth-layout/auth.module').then(m => m.AuthModule),
    // canActivate: [LoginGuard]
  },
  {
    path: '',//id is orgID
    loadChildren: () => import('../app//main/vehicle-booking/vehicle.module').then(m => m.VehicleModule),
    // canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
