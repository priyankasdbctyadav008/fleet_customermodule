import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from './services/api/api.service';
import { HttpService } from './services/api/http.service';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [TranslatePipe]
})
export class AppComponent {
  title: string = 'fleet_customerModule';
  index: any;
  isLoading: any = false;
  constructor(private route: ActivatedRoute, private api: ApiService, private httpService: HttpService, public translate: TranslateService) {

  }

  ngOnInit() {
    this.api.successMsg$.subscribe(
      (message) => {
        let data = JSON.parse(JSON.stringify(message));
        // this.title = data.message;
        setTimeout(() => {
          this.title = data.message;
          console.log('this.title: ', this.title);
        });
      }
    );
  }

  ngAfterViewInit() {
    // Update the boolean property here
    this.httpService.isLoading.subscribe((value) => {
      this.isLoading = value;
      console.log('this.isLoading: ', this.isLoading);
    })
  }


  getTitleClass(): string {
    return this.title.length === 2 || this.title.length === 4 ? 'contentBox1' : 'contentBox';
  }
}
